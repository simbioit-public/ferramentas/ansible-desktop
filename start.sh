#!/bin/bash

echo "Script criado pela Simbio IT para automatizar um desktop para trabalhar com devops ! "


sudo zypper refresh 
sudo zypper install --assume-yes ansible git

git clone https://gitlab.com/simbioit-public/ferramentas/ansible-desktop.git

cd ansible-desktop/ansible

ansible-playbook -i localhost main.yml